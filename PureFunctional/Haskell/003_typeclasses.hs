{-
	Keep in mind that:

	HASKELL					JAVA
	Class					Interface
	Type					Class
	Value					Object
	Method					Method

	Inheritance allows us to automatically implement some operators! This is the black magic of the compiler.	
-}

-- class Eq
-- gives the implementation of '==' (and '/=' or '!=' by negating the first operator)

-- class Show
-- the method show it's used for printing

-- We can specify if a type used in parametric polymorphism fashion has some operators.

palindromeCheck :: (Eq a) => [a] -> Bool
palindromeCheck [] = False
palindromeCheck lst = if lst == reverse lst
                      then True
                      else False