{-
	Lists in Haskell are the most basic data type.
	They can be built like:
		[1,2,3,4,5]
		1 : 2 : 3 : 4 : 5 : [] where the last [] is mandatory!
-}

-- extract the last element of a list
-- null lst returns whether a list is empty or not
lastElement :: [a] -> a
-- we use the infix : operator to separate head (car) and tail (cdr) of a list
lastElement (x:xs) = if null xs
                     then x
                     else lastElement xs

-- we also have the 'last' function
lastElement' :: [a] -> a
lastElement' lst = last lst

{-
	Some useful functions are:
	head lst 		->	extract first element
	last lst 		->	extract last element
	tail lst 		->	extract the tail after current element (the xs in x:xs)
	init lst 		->	extract all elements except the last one
-}

-- extract k-th element of a list

-- we need a support function to iterate until the k-th element. Remember that lists
-- starts from 0!
kthSupport :: [a] -> Int -> Int -> a
-- we need the k-1 since lists start from 0
kthSupport (x:xs) k tmp_k = if (k-1) == tmp_k
                            then x
                            else kthSupport xs k (tmp_k+1)

kth :: [a] -> Int -> a
kth lst k = kthSupport lst k 0

-- But we have a function ready to do it
kth' lst k = lst !! (k-1)

-- reverse a list
reverseList :: [a] -> [a]
reverseList [] = []
reverseList (x:xs) = (reverseList xs) ++ [x]

{- 	HOW TO COMPOSE A LIST?
	Haskell has no side effects, so we must exploit recursion to build a list. But there are 
	two ways of build a partial list:

	: operator
	it takes an element and a list, a:lst (like x:xs). The second element MUST be a list and cannot be a single value.
	The first element MUST be a single element and cannot be a list. As a result, : should be used when we're scanning
	through a list, it's not for building it.

	++ operator
	the concatenation operator works with lists, NOT single values. We can though encapsulate a single value inside a 
	list:  x -> [x]  and exploit ++

-}

-- as always, we already have the reverse function
--palindromeCheck :: [a] -> Bool
--palindromeCheck [] = False
--palindromeCheck lst = if lst == reverse lst
--                      then True
--                      else False
-- BUT IT CANNOT BE DONE!!!!! There is the == operator for type a?
-- We do not know!