describeLetter :: Char -> String
-- if-then-else is simple
describeLetter c | (c == 'a') || (c == 'A') = if c == 'a'
                                              then "CASE a"
                                              else "CASE A"
-- case var of ...
-- pattern -> consequences
-- the _ is the don't care or the default of the case. Note that case gets
-- evaluated sequentially from top to bottom, that's why the last one is a 
-- don't care, it means all other patterns have found no match.
                 | (c >= 'b') && (c <= 'z') = case c of
                                              'b' -> "The almighty letter b"
                                              _ -> "Puny letters"