-- One of the most powerful tools to build a list respecting some boundaries

-- basic comprehensions
l1 = [x | x <- [1,2,3,4,5] ]    -- l1 = [1,2,3,4,5]
l2 = [x | x <- [1..3] ]         -- l2 = [1,2,3]
l3 = [x | x <- [2,4..10] ]      -- l3 = [2,4,6,8,10]

-- divisors of 9 except 81
l4 = [x | x <- [1..100], (mod x 9) == 0, x /= 81]

-- more than one value
l5 = [ x*y | x <- [1..5], y <- [1..5]]

-- only uppercases
ltmp6 = "Fuck you Haskell"
l6 = [x | x <- ltmp6, elem x ['A'..'Z']]

{-
	Through comprehensions, we can filter a list almost for free! Just build the wanted list in the right part and
	select only the wanted elements through the <- pattern.
-}