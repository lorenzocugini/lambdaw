addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

-- the underscore stands for don't care.
addThreeIfFirstIsNotZero :: Int -> Int -> Int -> Int
addThreeIfFirstIsNotZero 0 _ _ = (error "First element is zero.")
addThreeIfFirstIsNotZero x y z = addThree x y z

-- the '|' is the boolean guard. Otherwise is a keyword for like the default of a case.
isEven :: Int -> Bool
isEven num | mod num 2 == 0 = True
           | otherwise = False

lengthOfList :: [a] -> Int
lengthOfList [] = 0
lengthOfList (x:xs) = 1 + lengthOfList xs