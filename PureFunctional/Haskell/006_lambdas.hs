-- lambdas are anonymous functions

addOne :: Int -> Int
addOne = \x -> x+1

addNums :: Int -> Int -> Int
addNums = \x y -> x+y