{-
	We must define some new types to work with
-}

-- data Bool = False | True

-- before the '=' is the TYPE CONSTRUCTOR, the second part is the DATA CONSTRUCTOR.
-- Type and Data Constructors live in separate namespaces.

-- Above, Bool is kind of an enumeration, there's no free variable to valorize.

data Point a = Point a a
-- Point is a container of type a. We can specify
-- 		Point Int
--		Point Float
--		Point [a]
--		Point (Point a)
--		...

-- The second part means that a constructor for Point a must take two inputs of type a

printPoint :: (Show a) => (Point a) -> String
printPoint (Point x y) = (show x) ++ " " ++ (show y)
-- call through ghci with printPoint (Point .. ..)



-- sometimes we want to inherit an operator, we can:
data PointEq a = PointEq a a deriving (Eq)

-- but we also can explicitly implement said methods
data PointTwo a = PointTwo a a

pointEquivalence :: (Eq a) => (PointTwo a) -> (PointTwo a) -> Bool
pointEquivalence (PointTwo x1 y1) (PointTwo x2 y2) = (x1 == x2) && (y1 == y2)