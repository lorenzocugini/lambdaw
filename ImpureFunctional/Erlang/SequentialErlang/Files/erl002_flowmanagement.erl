-module(erl002_flowmanagement).
-export([start/2, booleanguard/2]).

start(A,B) ->
% The construct is if pattern -> what to do; ...; true -> what to do (true is the default) end.
	if 	A == B ->
			io:fwrite("A=B~n");	% ~n stands for newline
		A > B ->
			io:fwrite("A>B~n");
		true ->
			io:fwrite("A<B~n")
	end.

booleanguard(0,_) -> io:fwrite("First is zero.~n");
booleanguard(A,B) when A > 0 ->
	io:fwrite("A is bigger than zero.~n");
booleanguard(A,B) when A < 0 ->
	io:fwrite("A is smaller than zero.~n").