-module(erl004_lambdas).
-import(math,[pow/2]).
-export([start/1]).

start(N) ->
	A = 1,
	B = 2,

	F = fun() -> A*B end,

	F(), %this doesn't get printed

	PowerOfThree = fun(X) -> pow(3,X) end,
	PowerOfThree(N) %this does get printed
	.