% The name of the module must match the name of the file. Note that the final dot '.' is mandatory.
-module(erl001_basicfunctions).

% Functions must be exported, the number after the '/' is the number of inputs required.
-export([factorial/1]).
% All export must be above all functions' definition.
-export([addThreeNumbers/3]).



factorial(0) -> 1;
% ; means that the current function still has a pattern
factorial(N) -> N * factorial(N-1).
% . means the we don't have any other pattern for the current function

addThreeNumbers(X, Y, Z) -> X+Y+Z.