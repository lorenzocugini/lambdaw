-module(erl003_lists).
% imports are needed to use external libraries
-import(lists, [last/1]).
-export([start/1, listlibrary/1]).

% Lists are written like Haskell's [1,2,3,4,5]

% Assuming start takes a list as input
start( [] ) ->
	die;  %this is an atom, 'die' will be printed out to stdout

start([X|XS]) ->
	io:fwrite("-> ~p~n", [X]),
	start(XS).

listlibrary(List) ->
	% ~p prints whatever parameter has been passed
	io:fwrite("Last element: ~p~n", [last(List)]).