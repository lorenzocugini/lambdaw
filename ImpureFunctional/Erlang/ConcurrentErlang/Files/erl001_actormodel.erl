-module(erl001_actormodel).
% You must export the functions passed to processes.
-export([start/0, loop/0]).

% A process is represented with <0, pid, 0>

start() ->
	% ?MODULE retrieves the current module
	% loop is the function that the new process will execute
	% with []/no parameters passed
	Pid = spawn(?MODULE, loop, []),
	% Send a message to Pid saying my pid and the atom 'hello'
	Pid ! {self(), hello},
	%Kill your son.
	Pid ! stop.

loop() ->
	receive
		{Pid, hello} ->
			io:fwrite("Received an hello from ~p~n", [Pid]),
			loop();

		% Standard way to make a process die, by sending it a dying message
		stop ->
			ok	% print whatever atom
	end.