% Commands
% c(erl002_pingpong).
% erl002_pingpong:start(10).

-module(erl002_pingpong).

% I want to ping exactly N times.
-export([start/1]).
-export([ping/1, pong/0]).


% The register function allows the pid to be broadcasted through the alias registered.
start(PingNum) ->
	%register(pinging, spawn(?MODULE, ping, [PingNum])),
	%register(ponging, spawn(?MODULE, pong, [])).

	register(ponging, spawn(?MODULE, pong, [])),
	register(pinging, spawn(?MODULE, ping, [PingNum])).

ping(0) ->
	ponging ! {self(), match};
ping(N) ->
	ponging ! {self(), pings},

	receive
		{Pid, pongs} ->
			io:fwrite("~p) PING!~n", [Pid]),
			ping(N-1)
	end.


pong() ->
	receive
		{Pid, pings} ->
			io:fwrite("~p) PONG!~n", [Pid]),
			pinging ! {self(), pongs},
			pong(); %remember to call the procedure again, otherwise it stops!

		% The don'care '_' is because we receive something we're not using.
		% It's not mandatory, but unused variables yield warnings during compilation!
		{_, match} ->
			io:fwrite("Someone won.~n")
	end.